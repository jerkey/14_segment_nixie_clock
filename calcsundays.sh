# flattered and also disapointed that i did not see a better way to do this (on the commandline at least) when i searched AFTER i wrote it

echo -n "uint8_t secondsundayinmarch[] = {"
for year in $(seq 2000 2255) ; do
    secondline=$(cal march $year | head -n4 | tail -n1 | sed 's/\(..\).*/\1/')
    [ $secondline -eq 8 ] && echo -n $secondline || echo -n $(expr $secondline + 7) 
    echo -n ","
done
echo "0}; // date for year 2000-2255 and a zero at the end"

echo -n "uint8_t firstsundayinnovember[] = {"
for year in $(seq 2000 2255) ; do
    secondline=$(cal november $year | head -n4 | tail -n1 | sed 's/\(..\).*/\1/')
    [ $secondline -eq 8 ] && echo -n $(expr $secondline - 7) || echo -n $secondline 
    echo -n ","
done
echo "0}; // date for year 2000-2255 and a zero at the end"

exit
   November 2020      
Su Mo Tu We Th Fr Sa  
 1  2  3  4  5  6  7  
 8  9 10 11 12 13 14  
15 16 17 18 19 20 21  
22 23 24 25 26 27 28  
29 30                 
                      
   November 2021      
Su Mo Tu We Th Fr Sa  
    1  2  3  4  5  6  
 7  8  9 10 11 12 13  
14 15 16 17 18 19 20  
21 22 23 24 25 26 27  
28 29 30              
                      
   November 2022      
Su Mo Tu We Th Fr Sa  
       1  2  3  4  5  
 6  7  8  9 10 11 12  
13 14 15 16 17 18 19  
20 21 22 23 24 25 26  
27 28 29 30           
                      
https://www.timeanddate.com/time/zone/usa/los-angeles
2020	Sun, Mar 8 at 2:00 am	PST → PDT	+1 hour (DST start)	UTC-7h
 	Sun, Nov 1 at 2:00 am	PDT → PST	-1 hour (DST end)	UTC-8h
2021	Sun, Mar 14 at 2:00 am	PST → PDT	+1 hour (DST start)	UTC-7h
 	Sun, Nov 7 at 2:00 am	PDT → PST	-1 hour (DST end)	UTC-8h
2022	Sun, Mar 13 at 2:00 am	PST → PDT	+1 hour (DST start)	UTC-7h
 	Sun, Nov 6 at 2:00 am	PDT → PST	-1 hour (DST end)	UTC-8h
2023	Sun, Mar 12 at 2:00 am	PST → PDT	+1 hour (DST start)	UTC-7h
 	Sun, Nov 5 at 2:00 am	PDT → PST	-1 hour (DST end)	UTC-8h
2024	Sun, Mar 10 at 2:00 am	PST → PDT	+1 hour (DST start)	UTC-7h
 	Sun, Nov 3 at 2:00 am	PDT → PST	-1 hour (DST end)	UTC-8h
2025	Sun, Mar 9 at 2:00 am	PST → PDT	+1 hour (DST start)	UTC-7h
 	Sun, Nov 2 at 2:00 am	PDT → PST	-1 hour (DST end)	UTC-8h
2026	Sun, Mar 8 at 2:00 am	PST → PDT	+1 hour (DST start)	UTC-7h
 	Sun, Nov 1 at 2:00 am	PDT → PST	-1 hour (DST end)	UTC-8h
2027	Sun, Mar 14 at 2:00 am	PST → PDT	+1 hour (DST start)	UTC-7h
 	Sun, Nov 7 at 2:00 am	PDT → PST	-1 hour (DST end)	UTC-8h
2028	Sun, Mar 12 at 2:00 am	PST → PDT	+1 hour (DST start)	UTC-7h
 	Sun, Nov 5 at 2:00 am	PDT → PST	-1 hour (DST end)	UTC-8h
2029	Sun, Mar 11 at 2:00 am	PST → PDT	+1 hour (DST start)	UTC-7h
 	Sun, Nov 4 at 2:00 am	PDT → PST	-1 hour (DST end)	UTC-8h
