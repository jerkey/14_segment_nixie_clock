#define PST -8
#define PDT -7
#define MARCH 3
#define NOVEMBER 11

uint8_t secondsundayinmarch[] = {12,11,10,9,14,13,12,11,9,8,14,13,11,10,9,8,13,12,11,10,8,14,13,12,10,9,8,14,12,11,10,9,14,13,12,11,9,8,14,13,11,10,9,8,13,12,11,10,8,14,13,12,10,9,8,14,12,11,10,9,14,13,12,11,9,8,14,13,11,10,9,8,13,12,11,10,8,14,13,12,10,9,8,14,12,11,10,9,14,13,12,11,9,8,14,13,11,10,9,8,14,13,12,11,9,8,14,13,11,10,9,8,13,12,11,10,8,14,13,12,10,9,8,14,12,11,10,9,14,13,12,11,9,8,14,13,11,10,9,8,13,12,11,10,8,14,13,12,10,9,8,14,12,11,10,9,14,13,12,11,9,8,14,13,11,10,9,8,13,12,11,10,8,14,13,12,10,9,8,14,12,11,10,9,14,13,12,11,9,8,14,13,11,10,9,8,13,12,11,10,9,8,14,13,11,10,9,8,13,12,11,10,8,14,13,12,10,9,8,14,12,11,10,9,14,13,12,11,9,8,14,13,11,10,9,8,13,12,11,10,8,14,13,12,10,9,8,14,12,11,10,9,14,13,12,11,0}; // date for year 2000-2255 and a zero at the end
uint8_t firstsundayinnovember[] = {5,4,3,2,7,6,5,4,2,1,7,6,4,3,2,1,6,5,4,3,1,7,6,5,3,2,1,7,5,4,3,2,7,6,5,4,2,1,7,6,4,3,2,1,6,5,4,3,1,7,6,5,3,2,1,7,5,4,3,2,7,6,5,4,2,1,7,6,4,3,2,1,6,5,4,3,1,7,6,5,3,2,1,7,5,4,3,2,7,6,5,4,2,1,7,6,4,3,2,1,7,6,5,4,2,1,7,6,4,3,2,1,6,5,4,3,1,7,6,5,3,2,1,7,5,4,3,2,7,6,5,4,2,1,7,6,4,3,2,1,6,5,4,3,1,7,6,5,3,2,1,7,5,4,3,2,7,6,5,4,2,1,7,6,4,3,2,1,6,5,4,3,1,7,6,5,3,2,1,7,5,4,3,2,7,6,5,4,2,1,7,6,4,3,2,1,6,5,4,3,2,1,7,6,4,3,2,1,6,5,4,3,1,7,6,5,3,2,1,7,5,4,3,2,7,6,5,4,2,1,7,6,4,3,2,1,6,5,4,3,1,7,6,5,3,2,1,7,5,4,3,2,7,6,5,4,0}; // date for year 2000-2255 and a zero at the end

int daylight_savings_offset(uint16_t year, uint8_t month, uint8_t day, int8_t hour) {
  if ((month < MARCH) || (month > NOVEMBER)) return PST;
  if ((month > MARCH) && (month < NOVEMBER)) return PDT;
  if (month == MARCH) {
    uint8_t switchdate = secondsundayinmarch[year - 2000];
    if (day < switchdate) return PST;
    if (day > switchdate) return PDT;
    if (day == switchdate) {
      if (hour + PST < 2) return PST;
      if (hour + PST > 2) return PDT;
    }
  }
  if (month == NOVEMBER) {
    uint8_t switchdate = firstsundayinnovember[year - 2000];
    if (day < switchdate) return PDT;
    if (day > switchdate) return PST;
    if (day == switchdate) {
      if (hour + PDT < 2) return PDT;
      if (hour + PDT > 2) return PST;
    }
  }
}
